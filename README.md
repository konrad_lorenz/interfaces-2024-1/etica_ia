# CODED BIAS:
**Análisis Ético y Técnico de la IA en Diseño de UI/UX: Un Estudio Basado en 'Coded Bias' y Casos Reales**

## Objetivos del Taller:
1. Analizar las implicaciones éticas y técnicas de la IA en el diseño de interfaces de usuario a través de la película "Coded Bias".
2. Contrastar los temas de la película con un caso real de aplicación de IA en UI/UX.
3. Redactar un artículo en formato IEEE que integre análisis teórico y práctico con referencias académicas obligatorias.

## Materiales Necesarios:
- Acceso a la película "Coded Bias".
- Acceso a bases de datos académicas y recursos en línea para investigación de casos reales y teorías relevantes.

## Descripción del Taller:

### Preparación y Visionado
- **Actividad:** Ver la película "Coded Bias" para entender los desafíos éticos y técnicos de la IA.

### Investigación y Selección de Caso Real
- **Actividad:** Investigar y seleccionar un caso real que ilustre la implementación de IA en el diseño de UI/UX. Este caso debería reflejar alguno de los problemas éticos o técnicos discutidos en la película.
  
### Redacción del Artículo
- **Actividad:** Escribir un artículo en formato IEEE que incluya:
  - **Introducción:** Presentación del tema, relevancia de "Coded Bias", y breve descripción del caso real seleccionado.
  - **Desarrollo:**
    - Análisis de cómo "Coded Bias" presenta los desafíos de la IA en UI/UX.
    - Contraste con el caso real, destacando similitudes y diferencias en los componentes técnicos y éticos.
  - **Análisis Ético:** Evaluación de las implicaciones éticas en ambos casos, destacando cómo estos aspectos afectan el diseño y la percepción de la tecnología.
  - **Conclusión:** Reflexiones sobre cómo los diseñadores de UI/UX pueden abordar estos desafíos de manera ética y efectiva.
  - **Referencias:** Incluir citas y referencias académicas según el formato IEEE.
  - **Nota:** El artículo debe tener un mínimo de 2 hojas y sera trabajo individual (si se descubre plagio y/o copia se penalizara de manera drastica).

## Entrega:
- **Artículo en LaTeX:** El documento debe ser enviado en formato PDF, siguiendo las directrices de estilo de IEEE.

## Evaluación:
- **Cumplimiento del formato IEEE:** Correcta utilización del formato en la estructura y las referencias del documento.
- **Profundidad del análisis:** Calidad del análisis teórico y práctico, y cómo se integran los ejemplos de la película y el caso real.
- **Reflexión ética y técnica:** Claridad en la discusión de las implicaciones éticas y técnicas, y propuestas de soluciones o mejoras.
- **Calidad de las referencias:** Uso apropiado de fuentes académicas para respaldar argumentos y análisis.
